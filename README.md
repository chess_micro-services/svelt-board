# svelt-board

## Get started

Chess Board requires a SocketIO server 
https://gitlab.com/chess_micro-services/redis-socketIO-server 

Install the dependencies...

```bash
git clone "SocketIO-..._server" socketIO
cd socketIO
npm install
npm start
```
Clone this repo..

```bash
git clone https://gitlab.com/chess_micro-services/svelt-board svelt-board
cd chess-board
npm install
```

...then start [Rollup](https://rollupjs.org):

```bash
npm run dev
```

Navigate to [localhost:5000](http://localhost:5000). You should see your app running. Edit a component file in `src`, save it, and reload the page to see your changes.

By default, the server will only respond to requests from localhost. To allow connections from other computers, edit the `sirv` commands in package.json to include the option `--host 0.0.0.0`.
