import App from './src/App.svelte';

var app = new App({
	target: document.body
});

export default app;
