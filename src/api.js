import io from 'socket.io-client';
// const socket = io('localhost:9591');
const socket = io('www.kellydev.net:19591', {secure: true});

export { socket };
